<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class focus extends Model
{
	protected $primaryKey = 'fid';
	protected $table = 'focus';

	public function Student(  ) {
		return $this->belongsTo(student::class);
	}

	public function Subjects() {
		return $this->belongsToMany(subjects::class, 'focussubjects','fid','subid');
	}

}
