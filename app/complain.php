<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class complain extends Model
{
    protected  $primaryKey = 'comid';
    protected  $table =  'complaints';

	public function Staff() {
		return $this->hasOne(staff::class,'stid','stid');
    }

	public function Student() {
		return $this->belongsTo(student::class,'sid','sid');
    }
}
