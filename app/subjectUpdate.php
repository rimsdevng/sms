<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subjectUpdate extends Model
{
    protected $primaryKey = 'suid';
    protected $table = 'subjectupdates';

	public function Subject() {
		return $this->belongsTo(subjects::class,'subid','subid');
    }

	public function Teacher() {
		return $this->belongsTo(staff::class,'stid','stid');
    }
}
