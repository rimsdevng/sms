<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class file extends Model
{
    protected $primaryKey = 'flid';
    protected $table = 'files';

	public function Staff() {
		return $this->belongsTo(staff::class,'stid','stid');
    }

}
