<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class focusSubject extends Model
{
    protected  $primaryKey = 'fsid';
    protected $table = 'focussubjects';

    protected $guarded = [ ];
}
