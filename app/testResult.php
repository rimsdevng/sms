<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class testResult extends Model
{
    protected $primaryKey = 'trid';
    protected $table = 'testresults';

	public function Test() {
		return $this->belongsTo(test::class,'testid','testid');
    }

	public function Student() {
		return $this->belongsTo(student::class,'sid','sid');
    }
}
