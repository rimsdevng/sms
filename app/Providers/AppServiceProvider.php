<?php

namespace App\Providers;

use App\Mail\errorNotifier;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {


	    if (session()->has('parent')){
		    view()->share('parent',session()->get('parent')->pid);

	    }else{
	    	view()->share('parent','there is nothing');
	    }

	    if (session()->has('student')){
		    view()->share('student',session()->get('student'));
	    }

	    Bugsnag::registerCallback(function($report) {
	    	try{
	    		$report = json_encode($report);
			    Mail::to('toby.okeke@gmail.com')->send(new errorNotifier($report));
		    }catch (\Exception $exception){}

	    });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
