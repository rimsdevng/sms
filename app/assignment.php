<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class assignment extends Model
{
    protected $primaryKey = 'aid';
    protected $table = 'assignments';

	protected $dates=['created_at','updated_at','due'];

	public function Teacher() {
		return $this->belongsTo(staff::class,'stid','stid');
    }

	public function Subject() {
		return $this->belongsTo(subjects::class,'subid','subid');
    }

	public function Class() {
		return $this->hasOne(classes::class,'cid','cid');
    }

	public function Submission() {
		return $this->hasMany(assignmentSubmission::class,'aid','aid');
    }

}
