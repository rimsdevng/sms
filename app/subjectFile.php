<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subjectFile extends Model
{
    protected $primaryKey = 'sfid';
    protected $table = 'subjectfiles';

	public function Subject() {
		return $this->belongsTo(subjects::class,'subid','subid');
	}

	public function Teacher() {
		return $this->belongsTo(staff::class,'stid','stid');
	}

}
