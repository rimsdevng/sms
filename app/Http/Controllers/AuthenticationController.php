<?php

namespace App\Http\Controllers;

use App\confirmation;
use App\Mail\forgotPasswordMail;
use App\parents;
use App\staff;
use App\student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthenticationController extends Controller
{


	public function __construct() {}

	public function resetPassword(Request $request) {

		$token = $request->input('token');
		$email = $request->input('email');
		$role = $request->input('role');

		if(confirmation::where('email',$email)->where('token',$token)->where('role',$role)->count() <= 0){

			session()->flash('error','Invalid Token. Please attempt to reset your password again.');
			return redirect('/');
		}

		$confirmation = confirmation::where('email',$email)->where('token',$token)->where('role',$role)->first();

		$confirmation->delete();

		return view('resetPassword',[
			'change' => true,
			'role' => $role,
			'email' => $email
		]);

	}

	public function postResetPassword(Request $request) {

		$role = $request->input('role');
		$email = $request->input('email');
		$newPassword = $request->input('newPassword');

		if($role == 'staff'){
			$staff = staff::where('email',$email)->first();
			$staff->password = bcrypt($newPassword);
			$staff->save();

			session()->flash('success','Password Reset.');
			return redirect('staff/login');

		}

		if($role == 'student'){
			$student = student::where('email',$email)->first();
			$student->password = bcrypt($newPassword);
			$student->save();

			session()->flash('success','Password Reset.');
			return redirect('student/login');
		}

		if($role == 'parent'){
			$parent = parents::where('phone',$email)->first();
			$parent->password = bcrypt($newPassword);
			$parent->save();
			session()->flash('success','Password Reset.');
			return redirect('parent/login');

		}

		if($role == 'admin'){
			$admin = User::where('email',$email)->first();
			$admin->password = bcrypt($newPassword);
			$admin->save();

			session()->flash('success','Password Reset.');
			return redirect('login');
		}


		return redirect('/');

	}


	//Parent Authentication
	public function loginParent(  ) {
		if(session()->has('parent')) return redirect('parent/dashboard');

		return  view('dashboard.parent.login');
	}


	public function postLoginParent( Request $request ) {

		$username = $request->email;
		$password = $request->password;

		$parent = parents::where('email',$username)->orWhere('phone',$username)->get();

		if(count($parent) <=  0){ // check that the email exists
			session()->flash('error',"Account doesn't exist");
			return redirect()->back();
		}

		$parent = parents::where('email',$username)->orWhere('phone',$username)->first();

		if(password_verify($password,$parent->password)){
			$parent->role = 'parent';
			session()->put('parent',$parent);

			return redirect('parent/dashboard');
		}else {
			session()->flash( 'error', 'Password is incorrect' );

			return redirect()->back();
		}


	}




	//Staff Authentication
	public function loginStaff() {
		if(session()->has('staff')) return redirect('staff/dashboard');

		return view('dashboard.staff.login');
	}

	public function postLoginStaff( Request $request ) {
		$email = $request->email;
		$password = $request->password;

		$staff =  staff::where('email',$email)->get();

		if(count($staff) <=  0){ // check that the email exists
			session()->flash('error',"Account doesn't exist");
			return redirect()->back();
		}

		$staff =  staff::where('email',$email)->first();

		if (password_verify($password,$staff->password)){
			$staff->role = 'staff';
			$staff->Desgination;
			session()->put('staff',$staff);

			return redirect('staff/dashboard');
		}else{
			session()->flash('error','Password is incorrect');
			return redirect()->back();
		}

	}



	//Student Authentication
	public function loginStudent(  ) {
		if(session()->has('student')) return redirect('student/dashboard');
		return view('dashboard.student.login');
	}


	public function postLoginStudent(Request $request) {
		$username = $request->email;
		$password = $request->password;
		$student = student::where('email',$username)->get();

		if(count($student) <=  0){ // check that the username exists

			session()->flash('error',"Account doesn't exist");
			return redirect()->back();

		}

		$student = student::where('email',$username)->first();

		if(!isset($student->Class)){
			session()->flash('error','You are not assigned to any class yet. Please contact IT');
			return redirect()->back();
		}

		if (password_verify($password,$student->password)){
			$student->role = 'student';
			$student->Subjects;
			$student->class = $student->Class->name;

			session()->put('student',$student);
			return redirect('student/dashboard');
		}else{
			session()->flash('error','Password is incorrect');
			return redirect()->back();
		}

	}


	public function logOutUser(Request $request,$user ) {
		 session()->remove($user);
		 if ($user == 'parent'){
		 	return redirect('parent/login');
		 }elseif ($user == 'staff'){
			 return redirect('staff/login');

		 }elseif ($user == 'student'){
			 return redirect('student/login');
		 }
	}


	public function changePassword() {

		if(auth()->check()) return view('changePassword');
		if(session()->has('parent')) return view('dashboard.parent.changePassword');
		if(session()->has('staff')) return view('dashboard.staff.changePassword');
		if(session()->has('student')) return view('dashboard.student.changePassword');
		return redirect('/');
	}


	public function postChangePassword( Request $request ) {
		$currentPassword = $request->input('currentPassword');
		$newPassword = $request->input('newPassword');
		$confirmPassword = $request->input('confirmPassword');

		if($newPassword != $confirmPassword){
			session()->flash('error','Confirmed password does not match your new password. Please try again.');
			return redirect()->back();
		}



		if(auth()->check())
		{
			$user = User::find(auth()->user()->uid);

			if(!password_verify($currentPassword, $user->password)) {
				session()->flash('error','Current Password is wrong');
				return redirect()->back();
			}

			$user->password = bcrypt($newPassword);
			$user->save();
			session()->flash('success','Password Changed.');
			return redirect()->back();
		}

		if(session()->has('parent')){
			$pid = session()->get('parent')->pid;
			$parent = parents::find($pid);

			if(!password_verify($currentPassword, $parent->password)) {
				session()->flash('error','Current Password is wrong');
				return redirect()->back();
			}

			$parent->password = bcrypt($newPassword);
			$parent->save();
			session()->flash('success','Password Changed.');
			return redirect()->back();
		}

		if(session()->has('staff')){
			$stid = session()->get('staff')->stid;
			$staff = staff::find($stid);

			if(!password_verify($currentPassword, $staff->password)) {
				session()->flash('error','Current Password is wrong');
				return redirect()->back();
			}

			$staff->password = bcrypt($newPassword);
			$staff->save();
			session()->flash('success','Password Changed.');
			return redirect()->back();
		}

		if(session()->has('student')){
			$sid = session()->get('student')->sid;
			$student = student::find($sid);

			if(!password_verify($currentPassword, $student->password)) {
				session()->flash('error','Current Password is wrong');
				return redirect()->back();
			}

			$student->password = bcrypt($newPassword);
			$student->save();
			session()->flash('success','Password Changed.');
			return redirect()->back();
		}


		return redirect()->back();
	}




	public function staffForgotPassword() {
		return view('dashboard.staff.forgotPassword');
	}

	public function postStaffForgotPassword(Request $request) {
		$email = $request->input('email');
		$token = Str::random(24);

		$confirmation = new confirmation();
		$confirmation->email = $email;
		$confirmation->token = $token;
		$confirmation->role = "staff";
		$confirmation->save();


		Mail::to($email)->send(new forgotPasswordMail($token, $email,"staff"));
		session()->flash('success','Check your mail for a password reset link.');
		return redirect()->back();
	}


	public function studentForgotPassword() {
		return view('dashboard.student.forgotPassword');
	}

	public function postStudentForgotPassword(Request $request) {
		$email = $request->input('email');
		$token = Str::random(24);

		$confirmation = new confirmation();
		$confirmation->email = $email;
		$confirmation->token = $token;
		$confirmation->role = "student";
		$confirmation->save();


		Mail::to($email)->send(new forgotPasswordMail($token, $email,"student"));
		session()->flash('success','Check your mail for a password reset link.');
		return redirect()->back();
	}


	public function parentForgotPassword() {
		return view('dashboard.parent.forgotPassword');
	}

	public function postParentForgotPassword(Request $request) {
		$phone = $request->input('email');
		$token = strtoupper(self::random_number(6));


		$confirmation = new confirmation();
		$confirmation->email = $phone;
		$confirmation->token = $token;
		$confirmation->role = "parent";
		$confirmation->save();

//		Mail::to($email)->send(new forgotPasswordMail($token, $email,"student"));

		$message = "Your password reset code is $token";
		$this->sendSms($phone,$message);
		session()->flash('success','Check your phone for a password reset code.');
		session()->flash('showReset',"true");
		session()->flash('resetPhone',$phone);
		return redirect()->back();
	}

	public function adminForgotPassword() {
		return view('forgotPassword');
	}

	public function postAdminForgotPassword(Request $request) {
		$email = $request->input('email');
		$token = Str::random(24);

		$confirmation = new confirmation();
		$confirmation->email = $email;
		$confirmation->token = $token;
		$confirmation->role = "admin";
		$confirmation->save();


		Mail::to($email)->send(new forgotPasswordMail($token, $email,"admin"));
		session()->flash('success','Check your mail for a password reset link.');
		return redirect()->back();
	}


	static  function random_number($length, $keyspace = '0123456789')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[random_int(0, $max)];
		}
		return $str;
	}


	function sendSms($phone,$message) {


		$message = urlencode( $message );
		file_get_contents( "https://www.bulksmsnigeria.com/api/v1/sms/create?dnd=2&api_token=rVY7mjk9AfG2CCx9KdzHkqB1CSVCoyOvNxEvKLdnhEVbtrtcZ7uM8ElPeC7S&from=HENDON&to=$phone&body=$message" );
	}




	}
