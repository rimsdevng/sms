<?php

namespace App\Http\Controllers;

use App\staff;
use App\subjects;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    //


	public function addSubject(  ) {
		$staffs = staff::all();
		return view('subject.add',[
			'staffs' => $staffs
		]);
	}

	public function postSubject(Request $request  ) {

		$subject  = new  subjects();
		$subject->name = $request->input('name');
		$subject->stid = $request->input('stid');

		$status = $subject->save();

		if ($status){
			session()->flash('success','Subject Added');
		}else{
			session()->flash('error','Something Went Wrong');
		}

		return redirect()->back();
	}

	public function Subjects(  ) {
		$subjects  = subjects::all();
		return view('subject.manage',[
			'subjects' => $subjects
		]);


	}


	public function changeSubjectTeacher( Request $request ) {
		$subid = $request->input('subid');
		$stid = $request->input('stid');

		$subject = subjects::find($subid);
		$subject->stid = $stid;
		$subject->save();

		session()->flash('success','Teacher Changed');
		return redirect()->back();
	}

	public function detail($subid) {

		$subject = subjects::findorfail($subid);
		$teachers = staff::all();

		return view('subject.details',[
			'subject' => $subject,
			'teachers' => $teachers
		]);


	}


}
