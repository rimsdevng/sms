<?php

namespace App\Http\Controllers;

use App\notifications;
use App\parents;
use App\staff;
use App\student;
use App\subjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{


	public function getNotification(  ) {
    	return view('notifications.add');
    }

	public function postNotification(Request $request  ) {

//    	return $request->all();

    	$notifications = new notifications();
    	$notifications->title = $request->input('title');
    	$notifications->content = $request->input('content');
    	$notifications->uid = Auth::user()->uid;
    	$status = $notifications->save();
    	if ($status){

//    		$data = (object)[
//    			'title'=> $notifications->title,
//			    'content' => $notifications->content
//		    ];
//		    $notifications->content


    		$this->sendMessage($notifications->title , $notifications->content);
    		session()->flash('success','Notification Sent sucessfully');
    		return redirect()->back();
	    }else{
    		session()->flash('error','Sorry, something went wrong');
    		return redirect()->back();
	    }

    }


	public function viewNotifications() {
    	$notifications = notifications::orderBy('created_at','desc')->paginate(20);

    	return view('notifications.manage',[
    		'notifications' => $notifications
	    ]);


    }


	public function detail($nid) {
    	$notification = notifications::find($nid);
    	return view('notifications.details',[
    		'notification'=> $notification
	    ]);
    }

	public function delete($nid) {
    	$notification = notifications::destroy($nid);
//		$notification->save();
		return redirect()->back();

    }


    //sending oneSignal Push
	function sendMessage($title, $message){
		$content = array(
			"en" => $message
		);

		$fields = array(
			'app_id' => "9589c6fe-4df9-4fbd-9701-d85bc78ea105",
			'included_segments' => array('All'),
			'data' => array("title" => $title),
			'contents' => $content
		);

		$fields = json_encode($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
			'Authorization: Basic OGE5OGRiMTYtYzMzZC00NmY5LThhMDYtNjUyYTg5MzE4NDQ0'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}


	public function logout() {

    	if(auth()->guest()) return redirect('/');
		auth()->logout();
		return redirect('/');
	}











}
