<?php

namespace App\Http\Middleware;

use Closure;


class parents
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if($request->session()->has('parent'))
		    return $next($request);
	    else
		    return redirect('parent/login');

    }
}
