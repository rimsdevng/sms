<?php

namespace App\Http\Middleware;

use Closure;

class student
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

	    if($request->session()->has('student'))
		    return $next($request);
	    else
		    return redirect('student/login');

    }
}
