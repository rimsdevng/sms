<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->increments('uid');
			$table->string('name');
			$table->string('email',191)->unique();
			$table->string('password');
			$table->rememberToken();
			$table->timestamps();
		});


		Schema::create('password_resets', function (Blueprint $table) {
			$table->string('email',191)->index();
			$table->string('token');
			$table->timestamp('created_at')->nullable();
		});

		Schema::create('confirmations', function(Blueprint $table){
			$table->increments('confid');
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
			$table->string('role')->nullable();
			$table->string('token');
			$table->timestamps();
		});

		Schema::create('subjectupdates', function(Blueprint $table){
			$table->increments('suid');
			$table->integer('subid');
			$table->integer('stid');
			$table->string('update',5000);
			$table->timestamps();
		});

		Schema::create('subjectfiles', function(Blueprint $table){
		    $table->increments('sfid');
		    $table->integer('subid');
		    $table->integer('stid');
		    $table->string('description',5000)->nullable();
		    $table->string('url',2000);
		    $table->timestamps();
		});

		Schema::create('files', function(Blueprint $table){
		    $table->increments('flid');
		    $table->integer('stid');
		    $table->string('name');
		    $table->string('url',2000);
		    $table->string('type');
		    $table->timestamps();

		});


		Schema::create('application', function (Blueprint $table){

			//details of child

			$table->increments('apid');
			$table->integer('uid')->nullable();
			$table->string('staffRole')->nullable();
			$table->string('surname')->nullable();
			$table->string('forenames')->nullable();
			$table->string('preferredName')->nullable();
			$table->string('dob')->nullable();
			$table->enum('gender',['Male','Female'])->nullable();
			$table->string('religionDenomination')->nullable();
			$table->string('nationality')->nullable();
			$table->string('homeAddress')->nullable();
			$table->string('homePhone')->nullable();
			$table->string('firstLanguage')->nullable();
			$table->string('proposedYear')->nullable();
			$table->string('accommodation')->nullable();
			$table->string('connection')->nullable();


			//school

			$table->string('currentSchool')->nullable();
			$table->timestamp('attendanceDate')->nullable();
			$table->string('headTeacher')->nullable();
			$table->string('headTeacherEmail')->nullable();
			$table->string('schoolPhone')->nullable();

			//parent/guardian

			$table->string('father')->nullable();
			$table->string('fatherTitle')->nullable();
			$table->string('fatherProfession')->nullable();
			$table->string('fatherIndustry')->nullable();
			$table->string('fatherEmployerName')->nullable();
			$table->string('fatherPhone')->nullable();
			$table->string('fatherEmail')->nullable();

			$table->string('mother')->nullable();
			$table->string('motherTitle')->nullable();
			$table->string('motherProfession')->nullable();
			$table->string('motherIndustry')->nullable();
			$table->string('motherEmployerName')->nullable();
			$table->string('motherPhone')->nullable();
			$table->string('motherEmail')->nullable();

			$table->string('parentsName')->nullable();
			$table->string('parentsAddress')->nullable();

			//hear about hendon

			$table->string('hearOfHendon')->nullable();
			$table->string('advertisement')->nullable();


			//others with parental duties

			$table->string('others')->nullable();
			$table->string('title')->nullable();
			$table->string('fname')->nullable();
			$table->string('sname')->nullable();
			$table->string('address')->nullable();
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->string('relationshipToChild')->nullable();
			$table->string('medicalConditions',5000)->nullable();


			//stages
			$table->enum('section',[1,2,3,4])->nullable();
			$table->enum('status',['admitted','pending'])->nullable();
			$table->timestamps();

		});


		Schema::create('students', function(Blueprint $table)
		{
			$table->increments('sid');
			$table->string('fname', 255);
			$table->string('sname', 255);
			$table->string('gender', 255);
			$table->string('dob', 255)->nullable();
			$table->string('image', 2000)->nullable();
			$table->string('address', 2000)->nullable();
			$table->string('email', 191)->nullable()->unique();
			$table->string('phone', 100)->nullable();
			$table->string('nationality', 255)->nullable();
			$table->string('religion')->nullable();
			$table->string('studentid', 255)->nullable();
			$table->string('session', 255)->nullable();
			$table->string('language')->nullable();
			$table->integer('serialNumber')->nullable();
			$table->integer('roomNumber')->nullable();
			$table->integer('bedNumber')->nullable();
			$table->string('house')->nullable();
			$table->string('password', 255);
			$table->integer('fid')->nullable();
			$table->integer('cid')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});


		Schema::create( 'parents', function ( Blueprint $table ) {
			$table->increments( 'pid' );
			$table->string( 'fname' );
			$table->string( 'sname' );
			$table->string( 'phone',191 )->unique();
			$table->string( 'email' );
			$table->string( 'relationship' );
			$table->string('image',2000);
			$table->string('password');
			$table->string( 'address', 2000 );
			$table->softDeletes();
			$table->timestamps();


		} );


		Schema::create( 'staff', function ( Blueprint $table ) {
			$table->increments( 'stid' );
			$table->string('staffid')->nullable();
			$table->string( 'title' );
			$table->string('fname');
			$table->string('sname');
			$table->string('dob');
			$table->string('image',2000);
			$table->enum( 'gender', [ 'male', 'female' ] );
			$table->string( 'residentialAddress', 2000 );
			$table->integer('did');
			$table->string( 'bio', 5000 );
			$table->string( 'stateOfOrigin' );
			$table->string('phone');
			$table->string('nationality');
			$table->string('category')->nullable();
			$table->string('password');
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'guarantors', function ( Blueprint $table ) {
			$table->increments( 'gid' );
			$table->integer( 'stid' );
			$table->string( 'gName' );
			$table->string('dob');
			$table->string('gProfession');
			$table->enum( 'gGender', [ 'male', 'female' ] );
			$table->string( 'gOfficeAddress', 2000 );
			$table->string( 'gStateOfOrigin' );
			$table->string('gPhone');
			$table->string('gLocation');
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'roles', function ( Blueprint $table ) {
			$table->increments( 'rlid' );
			$table->string('name');
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'comments', function ( Blueprint $table ) {
			$table->increments( 'cmid' );
			$table->integer('stid');
			$table->integer('sid');
			$table->integer('pid')->nullable();//newly added
			$table->boolean('fromparent')->nullable();//newly added
			$table->boolean('isstudentpermitted')->nullable();//newly added
			$table->string('comment');
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'designations', function ( Blueprint $table ) {
			$table->increments( 'did' );
			$table->string('category');
			$table->string('arm')->nullable(); //newly added
			$table->string('name',191 )->unique();
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'classes', function ( Blueprint $table ) {
			$table->increments( 'cid' );
			$table->string('name',191 )->unique();
			$table->integer('stid')->nullable();
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'subjects', function ( Blueprint $table ) {
			$table->increments( 'subid' );
			$table->string('name');
			$table->integer('stid');
			$table->string('cids')->nullable(); // array of classes
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'subjectsstudents', function ( Blueprint $table ) {
			$table->increments( 'ssid' );
			$table->integer('sid');
			$table->integer('subid');
			$table->softDeletes();
			$table->timestamps();

		});


		Schema::create('subjectteachers', function(Blueprint $table){
			$table->increments('subtid');
			$table->integer('subid');
			$table->integer('stid');
			$table->integer('cids')->nullable(); // array of classes
			$table->timestamps();
		});




		Schema::create( 'focus', function ( Blueprint $table ) {
			$table->increments( 'fid' );
			$table->string('name',191 )->unique();
			$table->integer('cid')->nullable(); // it will autofil the focus
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'focussubjects', function ( Blueprint $table ) {
			$table->increments( 'fsid' );
			$table->integer('fid');
			$table->integer('subid');
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'parentstudents', function ( Blueprint $table ) {
			$table->increments( 'psid' );
			$table->integer( 'pid' );
			$table->integer('sid');
			$table->softDeletes();
			$table->timestamps();

		} );




		// newly added schema start

		Schema::create( 'subjectresults', function ( Blueprint $table ) {
			$table->increments( 'srid' );
			$table->integer('sid');
			$table->integer('subid');
			$table->decimal('ce1')->nullable();
			$table->decimal('ce2')->nullable();
			$table->decimal('as1')->nullable();
			$table->decimal('cat1')->nullable();
			$table->decimal('ce3')->nullable();
			$table->decimal('ce4')->nullable();
			$table->decimal('as2')->nullable();
			$table->decimal('cat2')->nullable();
			$table->decimal('pr')->nullable();
			$table->decimal('exam')->nullable();
			$table->decimal('total')->nullable();
			$table->string('comments')->nullable();
			$table->softDeletes();
			$table->timestamps();

		} );



		Schema::create( 'results', function ( Blueprint $table ) {
			$table->increments( 'rid' );
			$table->integer('sid');
			$table->decimal('attendance')->nullable();
			$table->decimal('average')->nullable();
			$table->string('tComments')->nullable();
			$table->string('hComments')->nullable();
			$table->decimal('total')->nullable();
			$table->integer('position')->nullable();
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create('resultcomments', function(Blueprint $table){
		    $table->increments('rcid');
		    $table->string('comment',2000)->nullable();
		    $table->integer('number')->nullable();
		    $table->timestamps();
		    $table->softDeletes();

		});

		Schema::create( 'subjecttests', function ( Blueprint $table ) {
			$table->increments( 'subtid' );
			$table->integer('srid');
			$table->string('name');
			$table->decimal('score');
			$table->decimal('total');
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'settings', function ( Blueprint $table ) {
			$table->increments( 'setid' );
			$table->string('name');
			$table->decimal('value');
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'studenthealth', function ( Blueprint $table ) {
			$table->increments( 'shid' );
			$table->integer('sid');
			$table->string('comments');
			$table->integer('stid');
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'bills', function ( Blueprint $table ) {
			$table->increments( 'bid' );
			$table->string('name');
			$table->timestamp('due');
			$table->string('description');
			$table->decimal('amount');
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'billstudents', function ( Blueprint $table ) {
			$table->increments( 'bsid' );
			$table->integer('sid');
			$table->enum( 'status', [ 'paid', 'unpaid', 'partiallypaid'] );
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'payments', function ( Blueprint $table ) {
			$table->increments( 'payid' );
			$table->integer('bid');
			$table->decimal('amount');
			$table->string('reference');
			$table->integer('sid');
			$table->softDeletes();
			$table->timestamps();

		} );



		Schema::create( 'inventories', function ( Blueprint $table ) {
			$table->increments( 'inid' );
			$table->string('name');
			$table->string('description');
			$table->integer('quantity');
			$table->decimal('costPrice');
			$table->decimal('sellingPrice');
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'billinventories', function ( Blueprint $table ) {
			$table->increments( 'biid' );
			$table->integer('bid');
			$table->integer('inid');
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'billitemcollections', function ( Blueprint $table ) {
			$table->increments( 'bicid' );
			$table->integer('bid');
			$table->integer('inid');
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'billclubs', function ( Blueprint $table ) {
			$table->increments( 'blcid' );
			$table->integer('bid');
			$table->integer('clid');
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'requisitions', function ( Blueprint $table ) {
			$table->increments( 'rqid' );
			$table->string('name');
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'requisitiondetails', function ( Blueprint $table ) {
			$table->increments( 'rdid' );
			$table->integer('inid');
			$table->integer('rqid');
			$table->integer('quantity');
			$table->decimal('price');
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'assignments', function ( Blueprint $table ) {
			$table->increments( 'aid' );
			$table->string('name');
			$table->integer('stid');
			$table->integer('subid');
			$table->string('url');
			$table->string('description');
			$table->timestamp('due');
			$table->integer('cid');
			$table->decimal('total');
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'assignmentsubmissions', function ( Blueprint $table ) {
			$table->increments( 'asid' );
			$table->integer('aid');
			$table->integer('sid');
			$table->string('url');
			$table->string('description');
			$table->string('comment');
			$table->string('grade');
			$table->softDeletes();
			$table->timestamps();

		} );




		Schema::create( 'complaints', function ( Blueprint $table ) {
			$table->increments( 'comid' );
			$table->string('complaint');
			$table->integer('pid');
			$table->integer('stid')->nullable();
			$table->integer('sid')->nullable();
			$table->boolean('isPrincipalViewed');
			$table->boolean('isAdminViewed');
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'sickbays', function ( Blueprint $table ) {
			$table->increments( 'sbid' );
			$table->string('diagnosis');
			$table->string('treatment');
			$table->string('details');
			$table->integer('stid')->nullable();
			$table->integer('sid')->nullable();
			$table->softDeletes();
			$table->timestamps();

		} );


		Schema::create( 'transactions', function ( Blueprint $table ) {
			$table->increments( 'tid' );
			$table->integer('stid')->nullable();
			$table->decimal('total');
			$table->enum( 'paymentStatus', [ 'paid', 'unpaid', 'partiallypaid'] );
			$table->string( 'paymentMethod');
			$table->boolean('isCollected');
			$table->boolean('createdBy');
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'transactiondetails', function ( Blueprint $table ) {
			$table->increments( 'tdid' );
			$table->integer('inid');
			$table->integer('tid');
			$table->integer('quantity');
			$table->softDeletes();
			$table->timestamps();

		} );

		Schema::create( 'categories', function ( Blueprint $table ) {
			$table->increments( 'catid' );
			$table->string( 'name');
			$table->integer('stid');
			$table->string( 'description');
			$table->softDeletes();
			$table->timestamps();

		} );
		// end of newly added schema



		Schema::create('notifications', function (Blueprint $table) {
			$table->increments('nid');
			$table->integer('uid');
			$table->string('title');
			$table->string('content',5000);
			$table->timestamps();
		});


		Schema::create('billdetails', function (Blueprint $table) {
			$table->increments('bdid');
			$table->integer('bid');
			$table->string('item',5000);
			$table->string('unitprice');
			$table->string('quantity');
			$table->string('total');
			$table->timestamps();
		});


		Schema::create('complaints',function (Blueprint $table){
			$table->increments('comid');
			$table->integer('pid');
			$table->string('subject');
			$table->string('content',5000);
			$table->enum('status',['pending','resolved']);
			$table->timestamps();
		});


		Schema::create('tests', function (Blueprint $table){
			$table->increments('testid');
			$table->string('name');
			$table->string('description');
			$table->integer('duration');
			$table->string('startTime');
			$table->string('endTime');
			$table->integer('stid');
			$table->integer('subid');
			$table->integer('attempts')->default(1);
			$table->string('deadline')->nullable();
			$table->timestamps();
		});

		Schema::create('testclasses', function(Blueprint $table){
			$table->increments('tcid');
			$table->integer('testid');
			$table->integer('cid');
			$table->timestamps();
		});

		Schema::create('testresults', function(Blueprint $table){
		    $table->increments('trid');
		    $table->integer('testid');
		    $table->integer('sid');
		    $table->integer('score');
		    $table->integer('attempt');
		    $table->integer('total');
		    $table->timestamps();
		});

		Schema::create('questions', function (Blueprint $table){
			$table->increments('qid');
			$table->integer('testid');
			$table->integer('stid');
			$table->string('question');
			$table->string('option1');
			$table->string('option2');
			$table->string('option3');
			$table->string('option4');
			$table->string('option5')->nullable();
			$table->string('image',1000)->nullable();
			$table->integer('correctAnswer');
			$table->timestamps();
		});


		Schema::create('responses', function(Blueprint $table){
			$table->increments('resid');
			$table->integer('sid');
			$table->integer('qid');
			$table->integer('option'); // the number
			$table->integer('attempt');
			$table->timestamps();
		});

		Schema::create('attempts', function(Blueprint $table){
			$table->increments('atid');
			$table->integer('sid');
			$table->integer('testid');
			$table->timestamps();
		});

		Schema::create('news', function(Blueprint $table){
		    $table->increments('newsid');
		    $table->integer('stid');
		    $table->string('title');
		    $table->string('content',5000);
		    $table->timestamps();
		});

		Schema::create('news_images', function(Blueprint $table){
			$table->increments('niid');
			$table->integer('newsid');
			$table->string('url',2000);
			$table->timestamps();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::dropIfExists('users');
        Schema::dropIfExists('application');
        Schema::dropIfExists('password_resets');
	    Schema::dropIfExists('students');
	    Schema::dropIfExists('parents');
	    Schema::dropIfExists('staff');
	    Schema::dropIfExists('guarantors');
	    Schema::dropIfExists('roles');
	    Schema::dropIfExists('comments');
	    Schema::dropIfExists('subjects');
	    Schema::dropIfExists('designations');
	    Schema::dropIfExists('class');
	    Schema::dropIfExists('subjects');
	    Schema::dropIfExists('subjectsstudents');
	    Schema::dropIfExists('focus');
	    Schema::dropIfExists('focussubjects');
	    Schema::dropIfExists('results');
	    Schema::dropIfExists('parentstudents');
	    Schema::dropIfExists('subjectresults');
	    Schema::dropIfExists('resulttests');
	    Schema::dropIfExists('settings');
	    Schema::dropIfExists('studenthealth');
	    Schema::dropIfExists('bills');
	    Schema::dropIfExists('billstudents');
	    Schema::dropIfExists('billdetails');

	    Schema::dropIfExists('payments');
	    Schema::dropIfExists('inventories');
	    Schema::dropIfExists('billinventories');
	    Schema::dropIfExists('billitemcollections');
	    Schema::dropIfExists('billclubs');
	    Schema::dropIfExists('requisitions');
	    Schema::dropIfExists('requisitions');
	    Schema::dropIfExists('requisitiondetails');
	    Schema::dropIfExists('assignments');
	    Schema::dropIfExists('assignmentsubmissions');
	    Schema::dropIfExists('complaints');
	    Schema::dropIfExists('sickbays');
	    Schema::dropIfExists('transactions');
	    Schema::dropIfExists('transactiondetails');
	    Schema::dropIfExists('categories');
	    Schema::dropIfExists('notifications');
	    Schema::dropIfExists('complaints');
	    Schema::dropIfExists('tests');
	    Schema::dropIfExists('testclasses');
	    Schema::dropIfExists('questions');
	    Schema::dropIfExists('responses');
	    Schema::dropIfExists('attempts');
	    Schema::dropIfExists('news');
	    Schema::dropIfExists('news_images');



    }
}
