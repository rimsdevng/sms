@extends('layouts.admin')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Parent Detail</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div class="row">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="col-md-3 pull-left">
                            {{--                            <a href="{{url('student/'.$student->sid.'/edit')}}" class="btn btn-info padding-overlay"> Update record </a>--}}
                        </div>
                        <div class="col-md-3">
                            {{--<a href="{{url('admit-student/'.$application->apid)}}" class="btn btn-success padding-overlay"> Admit Student</a>--}}
                        </div>
                        <div class="col-md-3">
                            {{--                            <a href="{{url('make-student/'.$application->apid)}}" class="btn btn-danger padding-overlay pull-right"> Admit </a>--}}
                        </div>
                    </div>
                </div>
                <!-- /# row -->

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Notification Details</h4>

                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="user-photo m-b-30">
{{--                                                    <img class="img-responsive" src="{{$parent->image}}" alt="Photo Space" />--}}
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="user-profile-name dib">{{$notification->title}} </div>
                                                <div class="useful-icon dib pull-right">
{{--                                                    <span><a href="{{url('parent/'.$parent->pid.'/edit')}}" class="btn btn-danger"><i class="ti-pencil-alt"></i></a> </span>--}}
                                                    {{--<span><a href="#"><i class="ti-printer"></i></a></span>--}}
                                                    {{--<span><a href="#"><i class="ti-download"></i></a></span>--}}
                                                    <span><a href="#"><i class="ti-share"></i></a></span>
                                                </div>
                                                <div class="custom-tab user-profile-tab">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                            <div class="contact-information">
                                                                <div class="phone-content">
                                                                    <span class="contact-title"><b> Message:</b></span>
                                                                    <span class="phone-number">{{$notification->content}}</span>
                                                                </div>


                                                                {{--<div class="address-content">--}}
                                                                {{--<span class="contact-title">Home Address:</span>--}}
                                                                {{--<span class="mail-address">{{$student->address}}</span>--}}
                                                                {{--</div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>




                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection