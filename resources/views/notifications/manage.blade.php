@extends('layouts.admin')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Notifications List</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Notifications </h4>
                                    <div class="search-action">
                                        <div class="search-type dib">
                                            <input class="form-control input-rounded" placeholder="Search" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Title</th>
                                                <th>Content</th>
                                                <th>Created</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($notifications)>0)

                                                <?php $count = 1; ?>

                                                @foreach($notifications as $notification)
                                                    <tr>

                                                        <td>
                                                         #<?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$notification->title}}
                                                        </td>
                                                        <td>
                                                            {{$notification->content}}
                                                        </td>
                                                        <td>
                                                            {{$notification->created_at->diffForHumans()}}
                                                        </td>

                                                        <td>
                                                            <span><a href="{{url('notification/'.$notification->nid.'/detail')}}"><i class="ti-eye color-default"></i></a> </span>
                                                            {{--<span><a href="{{url('notification/'.$subjects->subid.'/edit')}}"><i class="ti-pencil-alt color-success"></i></a></span>--}}
                                                            {{--<span><a href="{{url('notification/'.$subjects->subid.'/delete')}}"><i class="ti-trash color-danger"></i> </a></span>--}}
                                                        </td>
                                                    </tr>
                                                    <?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no Notifications Sent </td>
                                                </tr>

                                            @endif


                                            </tbody>
                                        </table>

                                        <div class="pull-right">
                                            {{$notifications->links()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection