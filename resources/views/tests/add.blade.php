@extends('layouts.admin')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">

                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Tests</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>

                @include('notification')

                <div class="main-content">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-md-offset-2">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Add a Test</h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">
                                        <form class="form-horizontal" method="post" action="{{url('admin/add-test')}}">
                                            {{csrf_field()}}

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Title</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="title" placeholder="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Description</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" rows="5" name="description" placeholder="Enter a description"></textarea>
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Duration (Minutes)</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" name="duration" placeholder="">
                                                </div>
                                            </div>


                                            <p><b>Available between</b></p>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Start Time</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="startTime" class="form-control" name="startTime" placeholder="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">End Time</label>
                                                <div class="col-sm-10">
                                                    <input id="endTime" type="text" class="form-control" name="endTime" placeholder="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Deadline</label>
                                                <div class="col-sm-10">
                                                    <input id="deadline" type="text" class="form-control" name="deadline" placeholder="">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>

    <script>
        $(document).ready( function() {

            $('#startTime').datetimepicker();
            $('#endTime').datetimepicker();
            $('#deadline').datetimepicker();

        } );
    </script>



@endsection



































