<?php use Carbon\Carbon; ?>
@extends('layouts.admin')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Tests</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-body">

                                    <a href="{{url('upload-questions/' . $test->testid)}}" class="btn btn-primary pull-right">Upload Questions</a>
                                    <a href="{{url('add-question/' . $test->testid)}}" class="btn btn-primary pull-right" style="margin-right: 10px;">Add Question</a>

                                    <p class="page-title">Test - {{$test->name}}</p>
                                    <p>
                                        {{count($test->Questions)}} question(s) <br>
                                        Duration - {{$test->duration}} mins <br>

                                        Description - {{$test->description}} <br>
                                        Subject - {{$test->Subject->name}} <br>
                                        Available between <b>{{Carbon::createFromFormat("Y-m-d H:i:s",$test->startTime)->toDayDateTimeString()}}</b>
                                        and <b>{{Carbon::createFromFormat("Y-m-d H:i:s",$test->endTime)->toDayDateTimeString()}}</b> <br>

                                        Teacher - <a style="color:blue;" href="{{url('staff/' . $test->Staff->stid . '/detail')}}">
                                            {{$test->Staff->fname}} {{$test->Staff->sname}}
                                        </a>
                                    </p>
                                </div>
                            </div>

                            {{--<div class="card alert">--}}
                                {{--<div class="card-body" align="center">--}}

                                    {{--<form class="form-inline" method="post" action="{{url('add-focus-subject')}}">--}}
                                        {{--@csrf--}}
                                        {{--<input type="hidden" name="fid" value="{{$focus->fid}}" >--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label >Add Subject</label>--}}
                                            {{--<select class="form-control" name="subid" style="width: 200px;">--}}
                                                {{--@foreach($subjects as $subject)--}}
                                                    {{--<option value="{{$subject->subid}}">{{$subject->name}}</option>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}
                                        {{--</div>--}}

                                        {{--<button class="btn btn-success">Add</button>--}}
                                    {{--</form>--}}

                                {{--</div>--}}
                            {{--</div>--}}


                            <div class="card alert">
                                <div class="card-body">

                                    <h6>Questions</h6>

                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/n</th>
                                                <th>Question</th>
                                                <th>Created</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if( count($test->Questions) > 0 )

												<?php $count = 1; ?>

                                                @foreach($test->Questions as $question)
                                                    <tr>

                                                        <td>
                                                            #<?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$question->question}}
                                                        </td>
                                                        <td>
                                                            {{$question->created_at->diffForHumans()}}
                                                        </td>

                                                        <td>
                                                            <span><a href="{{url('question/' . $question->qid)}}"><i class="ti-eye color-default"></i></a> </span>
                                                            <span><a href="{{url('edit-question/' . $question->qid)}}"><i class="ti-file color-default"></i></a> </span>
                                                            <span><a href="{{url('delete-question/' . $question->qid )}}"><i class="ti-trash color-danger"></i> </a></span>
                                                        </td>
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4" style="text-align: center">There are no questions for this test</td>
                                                </tr>
                                            @endif




                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection