@extends('layouts.admin')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Parent Detail</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <a href="{{url('parents')}}" class="btn btn-secondary padding-overlay pull-left"> Back </a>
                    </div>
                </div>




                <div id="main-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Parent</h4>
                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="user-profile-name dib">{{$parent->fname}} </div>
                                                <div class="useful-icon dib pull-right">
                                                    <span><a href="{{url('parent/'.$parent->pid.'/edit')}}" class="btn btn-danger"><i class="ti-pencil-alt"></i></a> </span>
                                                    {{--<span><a href="#"><i class="ti-printer"></i></a></span>--}}
                                                    {{--<span><a href="#"><i class="ti-download"></i></a></span>--}}
                                                    <span><a href="#"><i class="ti-share"></i></a></span>
                                                </div>
                                                <div class="custom-tab user-profile-tab">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                            <div class="contact-information">
                                                                <div class="phone-content">
                                                                    <span class="contact-title"> Relationship:</span>
                                                                    <span class="phone-number">{{$parent->relationship}}</span>
                                                                </div>
                                                                <div class="phone-content">
                                                                    <span class="contact-title"> Phone:</span>
                                                                    <span class="phone-number">{{$parent->phone}}</span>
                                                                </div>
                                                                <div class="website-content">
                                                                    <span class="contact-title">Email:</span>
                                                                    <span class="contact-website">{{$parent->email}}</span>
                                                                </div>

                                                                <div class="gender-content">
                                                                    <span class="contact-title">Address:</span>
                                                                    <span class="gender">{{$parent->address}}</span>
                                                                </div>
                                                                {{--<div class="address-content">--}}
                                                                    {{--<span class="contact-title">Home Address:</span>--}}
                                                                    {{--<span class="mail-address">{{$student->address}}</span>--}}
                                                                {{--</div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>



                        <div class="main-content">
                            {{--<h3 style="color:silver">Children</h3>--}}
                            <div class="row">
                                <div class="col-lg-12 p-r-0 title-margin-right">
                                    <div class="page-header">
                                        <div class="page-title">
                                            <h1>
                                                @if(count($parent->students) > 1)
                                                Children
                                                    @else
                                                Child
                                                    @endif
                                            </h1>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                @foreach($parent->Students as $student)

                                <div class="col-lg-3">
                                    <div class="card alert">
                                        <div class="products_1 text-center">
                                            <div class="pr_img_price">
                                                <div class="product_img">

                                                    @if(!empty($student->image))
                                                        <img class="img-responsive"
                                                             src="{{$student->image}}"
                                                             alt="Student Image"/>
                                                    @else
                                                        <img class="img-responsive"
                                                             src="{{url('admin/assets/images/default-image.jpg')}}"
                                                             alt="Student Image"/>

                                                    @endif

                                                </div>

                                            </div>

                                            <div class="product_details">
                                                <div class="product_name">
                                                    <h5>{{$student->fname}} {{$student->sname}}</h5>
                                                </div>
                                                <div class="product_des">
                                        {{-- <p>{{$student->fid}}</p><br>--}}
                                                </div>
                                                <div class="prdt_add_to_cart">
                                                    <a href="{{url('student/'.$student->sid.'/detail')}}" type="button" class="btn btn-primary btn-rounded  m-l-5"> View</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /# card -->
                                </div>
                                <!-- /# column -->
                                @endforeach
                            </div>
                            <!-- /# row -->
                        </div>

                <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection