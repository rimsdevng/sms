<!doctype html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml"
      xmlns:v-bind="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Hendon CBT Portal</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <script src="{{url('admin/assets/js/vue.js')}}"></script>
    <script src="https://unpkg.com/axios@0.18.0/dist/axios.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 50px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>

    <style>
        .questions{
            margin-top:20vh;
        }

        .question{
            font-size: 30px;
            max-width:700px;
        }

        .option {
            font-size: 30px;

        }
        .time{
            color: green;
            font-family: sans;
            font-size: 40px;
            position: absolute;
            top:50px;
            right:50px;
        }

        .button{
            background-color: green;
            text-transform: uppercase;
            min-width: 200px;
            color:white;
            padding:15px;
            border-radius:10px;
            border:none;
            margin-top:20px;
            font-size: 20px;

        }
        b{
            font-size: 20px;font-weight: 800
        }
        .image{
            width:500px;
        }
        .navigation{
            position: relative;
            margin: 50px 0;
            width: 100%;
        }

        .option.selected{
            border: 1px solid black;
            width: 500px;
            border-radius: 50px;
        }
    </style>
</head>
<body >
<div id="app">
<div v-if="section == 'intro'" class="flex-center position-ref full-height">

    <div class="content">
        <div align="center">
            <img src="{{url('admin/assets/images/logo.png')}}">
        </div>
        <div class="title m-b-md">
           You are about to commence a test
        </div>

        <div >
            <b>
                {{$test->Subject->name}}
            </b>
                <br>

             <span style="font-size: 24px">

                {{$test->name}}

             </span>
            <br>

             <b>
                 Duration - {{$test->duration}} mins
            </b>
        </div>

        <div class="links">
            <a>Follow All instructions.</a>
        </div>

        <button v-on:click="startTest" class="button">Start</button>
    </div>
</div>

<div class="questions" align="center" v-if="section == 'testing'">

    <div class="top-right links">
        <a>Question @{{currentQuestion}} of @{{ questions.length }}</a>
    </div>

    <p class="time">@{{ hours }} : @{{ minutes }} : @{{ seconds }}</p>

    <h3 v-on:ticked="stopTest" class="question">
        @{{question.question}}
    </h3>

    <img v-if="question.image" v-bind:src="question.image" class="image">

    <div class="answers">
        <div v-for="(option,index) in question.options" v-on:click="selectOption(option,index)" class="option" >

            <input type="checkbox" v-bind:checked="currentOption === index"/>
            <label> @{{ option }}</label>
        </div>
    </div>

    <div class="navigation">
        <button v-on:click="previous" class="button">Previous</button>
        <button v-on:click="next" class="button" v-if="currentQuestion < questions.length">Next</button>
        <button v-on:click="confirmSubmit" class="button" v-if="currentQuestion == questions.length">Finish</button>
    </div>

</div>

    <div class="loading" v-if="section == 'loading'">
        <p align="center" style="font-size: 50px;">
            SUBMITTING. <span style="color:red;">DO NOT CLOSE THIS PAGE.</span>
        </p>

    </div>

    <div class="results" v-if="section == 'results'">

        <div class="content">
            <div align="center">
                <img src="{{url('admin/assets/images/logo.png')}}">
            </div>
            <div class="title m-b-md">
                Test Complete
            </div>

            <div>

                <span style="font-size: 24px">
                Your score @{{ score }} / @{{ questions.length }}

                 </span>
                <br>

            </div>

            <div class="links">
                <a>Thanks for writing</a>
            </div>

            <div class="navigation">
                <a href="{{url('student/subject/' . $test->subid)}}" class="button">Close</a>
            </div>
        </div>

    </div>

</div>



<script>

    <?php
//     required to collect data from controller to js
        echo"var questionData =  $questions;";
        echo"var duration =  $test->duration;";
        echo "var testid = $test->testid;";
        echo "var sid = " . session()->get('student')->sid . ";";
    ?>


    Vue.use(axios);
    var app = new Vue({
        el: '#app',
        data: {
            section : "intro",
            currentOption:null,
            currentQuestion : 0,
            question:                 {
                question: "What is 5 * 2?",
                image: "",
                options : [
                    5,10,20,30,40
                ]
            },
            answers : [],
            questions : questionData,
            duration : duration,
            testid : testid,
            sid : sid,
            // timer properties

            timer:"",
            wordString: {},
            start: "",
            end: "",
            interval: "",
            days:"",
            minutes:"",
            hours:"",
            seconds:"",
            message:"",
            statusType:"",
            statusText: "",
            url: "{{url('add-attempt')}}",
            stopUrl: "{{url('add-test-result')}}",
            score: 0


        },
        created() {
            // this.wordString = JSON.parse(this.trans);

            for(let i = 0; i < this.questions.length; i ++){
                this.answers.push(-1);
            }

            let data = {
                testid : this.testid,
                sid    : this.sid,
                _token: "{{csrf_token()}}"
            };

            let self = this;

            console.log(this.url);
            axios.post(self.url, data)
                .then(function (response) {

                    console.log(response);
                    response = response.data;

                    console.log(response.data);
                })
                .catch(function (error) {
                    console.log(error);
                });



        },
        methods: {
            startTest(){

                this.section = 'testing';
                this.next();

                // start the timer
                this.start = new Date().getTime();
                this.end = this.addMinutes(this.start,this.duration);

                // Update the count down every 1 second
                this.timerCount(this.start,this.end);
                this.interval = setInterval(() => {
                    this.timerCount(this.start,this.end);
                }, 1000);

            },
            selectOption(option,index){

                this.currentOption = index;
                this.answers[this.currentQuestion - 1] = index + 1;

            },
            next(){
                this.currentQuestion++;
                if(this.currentQuestion >= this.questions.length) this.currentQuestion = this.questions.length;
                this.question = this.questions[this.currentQuestion - 1];
                this.currentOption = this.answers[this.currentQuestion - 1] - 1;

                console.log(this.answers);

            },
            previous(){
                this.currentQuestion--;
                if(this.currentQuestion <= 0) this.currentQuestion = 1;
                this.question = this.questions[this.currentQuestion - 1];
                this.currentOption = this.answers[this.currentQuestion - 1] - 1;
            },
            confirmSubmit(){
                swal({
                    title: "Are you sure you want to submit?",
                    text: "Once submitted, you will not be able to continue this test!",
                    icon: "warning",
                    buttons: true,
                    // dangerMode: true,
                })
                    .then((willSubmit) => {
                        if (willSubmit) {
                            this.stopTest();
                            swal("Your test is being submitted.", {
                                icon: "success",
                            });
                        } else {
                            // just let them continue the test

                        }
                    });

            },
            stopTest(){


                let data = {
                    testid : this.testid,
                    sid    : this.sid,
                    questions : this.questions,
                    answers : this.answers,
                    total : this.questions.length,
                    _token: "{{csrf_token()}}"
                };

                let self = this;

                this.section = 'loading';
                console.log(this.stopUrl);
                axios.post(self.stopUrl, data)
                    .then(function (response) {


                        self.section = "results";
                        self.currentQuestion = 0;
                        self.score = response.data;

                        console.log(response);
                        response = response.data;

                        console.log(response.data);
                    })
                    .catch(function (error) {

                        setTimeout(function(){
                            console.log('retrying');
                            self.stopTest();
                        },300);

                        console.log(error);
                    });



            },
            addMinutes(date, minutes) {
                return new Date(date + minutes * 60000);
            },
            timerCount: function(start,end){
                // Get todays date and time
                var now = new Date().getTime();


                // Find the distance between now an the count down date
                var distance = start - now;
                var passTime =  end - now;

                if(distance < 0 && passTime < 0){
                    //for expired
                    this.stopTest();
                    clearInterval(this.interval);
                    return;

                }else if(distance < 0 && passTime > 0){
                    //tickers
                    this.calcTime(passTime);

                } else if( distance > 0 && passTime > 0 ){
                    // upcoming
                    this.calcTime(distance);
                }
            },
            calcTime: function(dist){

                // Time calculations for days, hours, minutes and seconds
                this.days = Math.floor(dist / (1000 * 60 * 60 * 24));
                this.hours = Math.floor((dist % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                this.minutes = Math.floor((dist % (1000 * 60 * 60)) / (1000 * 60));
                this.seconds = Math.floor((dist % (1000 * 60)) / 1000);
            }
        }
    });

</script>
</body>
</html>

