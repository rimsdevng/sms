@extends('dashboard.parent.layouts.app')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Children</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')
                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Your Children</h4>
                                </div>


                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Class</th>
                                                <th>Last Position</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($students) > 0)
                                                <?php $count = 1; ?>
                                                @foreach($students as $student)
                                                    <tr>
                                                        <td>
                                                         <?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$student->fname}} {{$student->sname}}
                                                        </td>
                                                        <td>
                                                            {{$student->Class->name}}
                                                        </td>
                                                        <td>
                                                            {{$student->Results[count($student->Results) - 1]->position}}
                                                        </td>
                                                        <td>
                                                            <span><a class="btn btn-success" href="{{url('parent/child/'.$student->sid)}}">View</a> </span>
                                                        </td>
                                                    </tr>
                                                    <?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                   <td colspan="4" style="color: silver; text-align: center; margin-top: 30px;">You have no registered children</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
