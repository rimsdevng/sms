@extends('dashboard.parent.layouts.app')
@section('content')




    <div class="content-wrap">
        <div class="main">
            {{--@include('notification')--}}

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Complain</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div class="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Add A Complain </h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            {{--<li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>--}}
                                            {{--<li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>--}}
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">
                                        <form class="form-horizontal" method="post" action="{{url('parent/complaint/add')}}">
                                            {{csrf_field()}}

                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Message</label>
                                                <div class="col-sm-8">
                                                    <textarea type="text" rows="5" class="form-control" name="content" placeholder="Enter your message and the school would contact you." required></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Student <br> (Use this if its related to a student)</label>
                                                <select name="sid"  class="col-sm-8" >
                                                    <option  disabled selected >Pick a Student</option>
                                                    @foreach($students as $student)
                                                        <option value="{{$student->sid}}" class="form-control">{{$student->fname}} {{$student->sname}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-offset-4 col-md-8">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>



                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>





@endsection
