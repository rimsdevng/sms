@extends('dashboard.parent.layouts.app')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>News</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">News</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <style>

                    .news img{
                        height:300px;
                    }
                    .news{

                    }

                    .news .title{
                        width:100%;
                    }
                    .news p {
                        font-size: 16px;
                        line-height: 32px;
                        word-wrap: break-word;
                    }

                    .news .sub-title{
                        font-size: 12px;

                    }
                </style>
                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">


                            @if(count($newsItems)>0)

		                        <?php $count = $from; ?>

                                @foreach($newsItems as $news)

                                    <div class="card alert">
                                        <div class="card-body">

                                            <div class="row news">

                                                <div class="col-md-4">
                                                    @if(count($news->Images) <= 0)
                                                        <img class="img-responsive"
                                                             src="{{url('admin/assets/images/default-image.jpg')}}"
                                                             alt="News Image"/>

                                                    @else
                                                        <img class="img-responsive"
                                                             src="{{$news->Images[0]->url}}"
                                                             alt="News Image"/>
                                                    @endif
                                                </div>

                                                <div class="col-md-8">
                                                    <h3 class="title">{{$news->title}}</h3> <br>
                                                    <h6 class="sub-title">Posted {{$news->created_at->toDayDateTimeString()}} ({{$news->created_at->diffForHumans()}})</h6>
                                                    <p class="description">
                                                        {!!  str_limit($news->content,400,'...') !!} </p>

                                                    <a style="float: right;margin-top:20px;" class="btn btn-success" href="{{url('parent/news/' . $news->newsid)}}">READ MORE</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

			                        <?php $count ++; ?>
                                @endforeach
                            @else


                                <p style="color: silver; text-align: center; margin-top: 30px;"> There are no news updates </p>


                            @endif



                            <div class="pull-right">
                                {{$newsItems->links()}}
                            </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                </div>
            </div>
        </div>
    </div>



@endsection
