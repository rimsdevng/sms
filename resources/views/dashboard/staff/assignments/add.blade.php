@extends('dashboard.staff.layouts.app')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">

                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Assignments</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>

                @include('notification')

                <div class="main-content">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-md-offset-2">
                            <div class="card alert" >
                                <div class="card-header">
                                    <h4>Add an Assignment</h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{url('staff/assignments/add')}}">
                                            {{csrf_field()}}

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Assignment Title</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="name" placeholder="" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Description</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" rows="5" name="description" placeholder="Enter a description"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Subject</label>
                                                <div class="col-sm-10">
                                                    <select name="subid" required class="form-control">
                                                        <option selected disabled>Pick a subject</option>
                                                        @foreach($subjects as $subject)
                                                            <option value="{{$subject->subid}}">{{$subject->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Class</label>
                                                <div class="col-sm-10">
                                                    <select name="cid" required  class="form-control">
                                                        <option selected disabled>Pick a class</option>
                                                        @foreach($classes as $class)
                                                            <option value="{{$class->cid}}">{{$class->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Add File</label>
                                                <div class="col-sm-10">
                                                    <input type="file" name="file">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Due Date</label>
                                                <div class="col-sm-10">
                                                    <input id="endTime" type="text" class="form-control" name="due" required placeholder="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Total</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" name="total" placeholder="Please type the total marks obtainable" required>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>

    <script>
        $(document).ready( function() {

            $('#startTime').datetimepicker();
            $('#endTime').datetimepicker({
                // numberOfMonths: 2,
                // showButtonPanel: true
            });

        } );
    </script>



@endsection



































