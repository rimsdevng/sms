<?php use Carbon\Carbon; ?>
@extends('dashboard.staff.layouts.app')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Questions</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-body">


                                    <a href="{{url('staff/question/' . ($question->qid + 1) )}}" class="btn btn-success pull-right" style="margin-right: 10px;">Next Question</a>

                                    <a href="{{url('staff/question/' . $question->qid . '/delete')}}" class="btn btn-danger pull-right" style="margin-right: 10px;">Delete Question</a>

                                    <a href="{{url('staff/question/' . ($question->qid - 1) )}}" class="btn btn-primary pull-right" style="margin-right: 10px;">Previous Question</a>



                                    <p class="page-title">Test - {{$question->Test->name}}</p>
                                    <p>
                                        Question - {{$question->question}}
                                    </p>

                                </div>
                            </div>

                            @if(isset($question->image))
                            <div class="card alert">
                                <div class="card-body" align="center">

                                    <img style="max-width: 400px;" src="{{$question->image}}">
                                </div>
                            </div>
                            @endif


                            <div class="card alert">
                                <div class="card-body">

                                    <h6>Options</h6>

                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Option</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                             <tr>
                                                <td>
                                                    1
                                                </td>
                                                <td>
                                                    {{$question->option1}}
                                                </td>
                                                <td>

                                                    @if($question->correctAnswer == 1)
                                                        <span class="badge badge-success">
                                                        Correct
                                                        </span>

                                                    @else

                                                        <span class="badge badge-danger">
                                                            Wrong
                                                        </span>

                                                    @endif
                                                </td>
                                            </tr>
                                             <tr>
                                                <td>
                                                    2
                                                </td>
                                                <td>
                                                    {{$question->option2}}
                                                </td>
                                                <td>
                                                    @if($question->correctAnswer == 2)
                                                        <span class="badge badge-success">
                                                        Correct
                                                        </span>

                                                    @else

                                                        <span class="badge badge-danger">
                                                            Wrong
                                                        </span>
                                                    @endif
                                                </td>
                                            </tr>

                                             <tr>
                                                <td>
                                                    3
                                                </td>
                                                <td>
                                                    {{$question->option3}}
                                                </td>
                                                <td>
                                                    @if($question->correctAnswer == 3)
                                                        <span class="badge badge-success">
                                                        Correct
                                                        </span>

                                                    @else

                                                        <span class="badge badge-danger">
                                                            Wrong
                                                        </span>
                                                    @endif
                                                </td>
                                            </tr>

                                             <tr>
                                                <td>
                                                    4
                                                </td>
                                                <td>
                                                    {{$question->option4}}
                                                </td>
                                                <td>
                                                    @if($question->correctAnswer == 4)

                                                        <span class="badge badge-success">
                                                        Correct
                                                        </span>

                                                    @else

                                                        <span class="badge badge-danger">
                                                            Wrong
                                                        </span>

                                                    @endif
                                                </td>
                                            </tr>

                                             <tr>
                                                 <td>
                                                     5
                                                 </td>
                                                 <td>
                                                     {{$question->option5}}
                                                 </td>
                                                 <td>
                                                     @if($question->correctAnswer == 5)
                                                         <span class="badge badge-success">
                                                        Correct
                                                        </span>

                                                     @else

                                                         <span class="badge badge-danger">
                                                            Wrong
                                                        </span>
                                                     @endif
                                                 </td>
                                             </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready( function() {

            $('#startTime').datetimepicker();
            $('#endTime').datetimepicker();

        } );
    </script>




@endsection