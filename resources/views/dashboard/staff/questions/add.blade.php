@extends('dashboard.staff.layouts.app')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">

                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="{{url('manage-tests')}}">Tests</a></li>
                                    <li class="active">Add Question</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>

                @include('notification')

                <div class="main-content">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-md-offset-2">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Add a Question for {{$test->name}}</h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{url('staff/add-question')}}">
                                            {{csrf_field()}}

                                            <input type="hidden" name="testid" value="{{$test->testid}}">

                                            {{--<div class="form-group">--}}
                                                {{--<label class="col-sm-2 control-label">Question</label>--}}
                                                {{--<div class="col-sm-10">--}}
                                                    {{--<input type="text" class="form-control" name="question" placeholder="">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Question</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" rows="5" name="question" required></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Image <br> <sup style="color: darkred;">(Optional)</sup></label>
                                                <div class="col-sm-10">
                                                    <input type="file" style="border:none" class="form-control" name="image" placeholder="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Option 1</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="option1" placeholder="" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Option 2</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="option2" placeholder="" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Option 3</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="option3" placeholder="" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Option 4</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="option4" placeholder="" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Option 5</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="option5" placeholder="">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Correct Answer</label>
                                                <div class="col-sm-10">
                                                    <select name="correctAnswer"  class="form-control" >
                                                        <option selected disabled>Pick an option</option>
                                                        <option value="1">Option 1</option>
                                                        <option value="2">Option 2</option>
                                                        <option value="3">Option 3</option>
                                                        <option value="4">Option 4</option>
                                                        <option value="5">Option 5</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                    <a href="{{url('test/' . $test->testid)}}" class="btn btn-warning">Back</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>

    <script>
        $(document).ready( function() {

            $('#startTime').datetimepicker();
            $('#endTime').datetimepicker();

        } );
    </script>



@endsection



































