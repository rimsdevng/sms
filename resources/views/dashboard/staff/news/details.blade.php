@extends('dashboard.staff.layouts.app')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>News</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">News</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <style>

                    .news img{
                        height:300px;
                    }
                    .news{

                    }

                    .news .title{
                        width:100%;
                    }
                    .news p {
                        font-size: 16px;
                        line-height: 32px;
                    }

                    .news .sub-title{
                        font-size: 12px;

                    }
                </style>
                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">

                            <a href="{{url('staff/news/' . $news->newsid .'/delete')}}" class="btn btn-danger pull-right">Delete</a>

                            <div class="card alert">
                                <div class="card-body">

                                    <div class="row news">

                                        <div class="col-md-4">
                                            @if(count($news->Images) <= 0)
                                                <img class="img-responsive"
                                                     src="{{url('admin/assets/images/default-image.jpg')}}"
                                                     alt="News Image"/>

                                            @else
                                                <img class="img-responsive"
                                                     src="{{$news->Images[0]->url}}"
                                                     alt="News Image"/>
                                            @endif
                                        </div>

                                        <div class="col-md-8">
                                            <h3 class="title">{{$news->title}}</h3> <br>
                                            <h6 class="sub-title">Posted {{$news->created_at->toDayDateTimeString()}} ({{$news->created_at->diffForHumans()}})</h6>
                                            <p class="description">
                                                {!!   $news->content !!}
                                            </p>

                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="row">

                                @foreach($news->Images as $image)
                                    <div class="col-md-3 image">
                                        <a data-fancybox="{{$news->title}}" data-caption="{{$news->title}}" href="{{$image->url}}"><img src="{{$image->url}}" class="img img-thumbnail"></a>
                                    </div>
                                @endforeach
                            </div>



                        </div>
                    </div>
                    <!-- /# column -->

                </div>
                <!-- /# row -->

            </div>
        </div>
    </div>
    </div>



@endsection
