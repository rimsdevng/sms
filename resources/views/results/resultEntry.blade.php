@extends('dashboard.staff.layouts.app')
@section('content')
    <div class="content-wrap">
        <div class="main">
            {{--@include('notification')--}}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Student</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div class="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Update Student</h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">
                                        <form class="form-horizontal" method="post" action="{{url('post-result-entry')}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="sid" value="{{$student->sid}}">
                                            <input type="hidden" name="subid" value="{{$subject->subid}}">


                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>First Name</label>
                                                            <input disabled type="text" class="form-control border-none input-flat bg-ash" name="fname" value="{{$student->fname}}" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Last Name</label>
                                                            <input disabled type="text" value="{{$student->sname}}" name="sname" class="form-control border-none input-flat bg-ash" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Class</label>
                                                            <input disabled type="text" value="{{$class->name}}" name="sname" class="form-control border-none input-flat bg-ash" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>First CA</label>
                                                                <input id="first"  type="text" name="ca1" value="{{$student->ca1}}" class="form-control border-none input-flat bg-ash" placeholder="First  assessment">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Second CA</label>
                                                            <input id="second"  type="text" name="ca2" value="{{$student->ca2}}" class="form-control border-none input-flat bg-ash" placeholder="Second  assessment">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Third CA</label>
                                                            <input id="third"  type="text" name="ca3" value="{{$student->ca3}}" class="form-control border-none input-flat bg-ash" placeholder="First  assessment">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Exams</label>
                                                            <input id="exam" type="text" name="exam" value="{{$student->exam}}" class="form-control border-none input-flat bg-ash" placeholder="Exam">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Total</label>
                                                            <input disabled id="total"

                                                                   @if(isset($result))
                                                                   value="{{$result->total}}"
                                                                   @endif

                                                                   type="text" name="total"  class="form-control border-none input-flat bg-ash" placeholder="Total">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <button class="btn btn-default btn-lg m-b-10 bg-warning border-none m-r-5 sbmt-btn" type="submit">Save</button>
                                            <button type="reset"  class="btn btn-default btn-lg m-b-10 m-l-5 sbmt-btn" >Clear</button>


                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>




    <script>

        $first = document.getElementById('first');
        $second = document.getElementById('second');
        $third = document.getElementById('third');
        $exam = document.getElementById('exam');
        // $total = document.querySelector('#total');

        // $total =
        // console.log($total);


        $total = 0;
        $first.on('keyUp',function(e){
            $total  += e.value;
            document.document.getElementById('total').value = $total;
        });





    </script>








@endsection