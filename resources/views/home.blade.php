@extends('layouts.admin')

@section('content')



    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Hello, <span>{{auth()->user()->name}}</span></h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Home</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                   {{--  the oldupdate  --}}
                    <div class="row">
                        {{--  <div class="col-lg-3">
                            <a href="{{url('manage-applications')}}">
                                <div class="card">
                                        <div class="stat-widget-eight">
                                        <div class="stat-header">
                                            <div class="header-title pull-left"> Applications </div>
                                            <div class="card-option drop-menu pull-right"><i class="ti-more-alt" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="stat-content">
                                            <div class="pull-left">
                                                <span class="stat-digit"> {{count($pendingApplications)}} pending</span>
                                                <small>({{count($applications)}} in total)</small>
                                            </div>
                                            <div class="pull-right">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-primary w-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">70% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>  --}}

                        {{-- The new update --}}

                        <div class="col-md-3">
                            <div class="card">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="ti-bar-chart f-s-22 color-warning border-warning round-widget"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h4>{{ count($applications) }}</h4>
                                        <h6>Applications</h6>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="card">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="ti-user f-s-22 color-primary border-primary round-widget"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h4>{{ count($staff) }}</h4>
                                        <h6>Staff</h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="card">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="ti-user f-s-22 color-success border-success round-widget"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h4>{{ count($parents) }}</h4>
                                        <h6>Parents</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="ti-user f-s-22 border-danger color-danger round-widget"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h4>{{ count($students) }}</h4>
                                        <h6>Students</h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="card">
                                <div class="media">
                                    <div class="media-left meida media-middle">
                                        <span><i class="ti-comment f-s-22 color-info border-info round-widget"></i></span>
                                    </div>
                                    <div class="media-body media-text-right">
                                        <h4>{{ count($subjects) }}</h4>
                                        <h6>Subjects</h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- End of the new updates --}}

                    </div>
                        <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection