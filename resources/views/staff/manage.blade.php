<?php use Illuminate\Support\Facades\Input; ?>
@extends('layouts.admin')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Staff</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Staff </h4>
                                    <div class="search-action">
                                        <div class="search-type dib">
                                            <form>
                                                <input class="form-control input-rounded" name="term" value="{{Input::get('term')}}" placeholder="Search" type="text">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Names</th>
                                                <th>Staff ID</th>
                                                <th>Class</th>
                                                <th>Gender</th>
                                                <th>Designation</th>
                                                <th>Joined</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($staffs)>0)

												<?php $count = $from; ?>

                                                @foreach($staffs as $staff)
                                                    <tr>

                                                        <td>
                                                            <?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$staff->fname}} {{$staff->sname}}
                                                        </td>
                                                        <td align="center">{{$staff->stid}}</td>
                                                        <td>
                                                            @if(strtolower($staff->category) == "academic" || strtolower($staff->category) == "teaching" )
                                                                @if(isset($staff->clid))
                                                                {{$staff->Class->name}}
                                                                    @else
                                                                No class Assigned
                                                                @endif

                                                                @else

                                                                Non Academic
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{title_case($staff->gender)}}
                                                        </td>
                                                        <td>
                                                            {{$staff->Designation->name}}
                                                        </td>
                                                        <td>
                                                            {{$staff->created_at->toDayDateTimeString()}}
                                                        </td>


                                                        <td class="actions">
                                                            <span><a href="{{url('staff/'.$staff->stid.'/detail')}}"><i class="ti-eye color-default"></i></a> </span>
                                                            <span><a href="{{url('staff/'.$staff->stid.'/edit')}}"><i class="ti-pencil-alt color-success"></i></a></span>
                                                            {{--<span><a href="#"><i class="ti-trash color-danger"></i></a></span>--}}
                                                        </td>
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else
                                                <tr>

                                                    <td colspan="6" style="color: silver; text-align: center; margin-top: 30px;"> There are no staff  </td>                                                </tr>

                                            @endif

                                            </tbody>
                                        </table>

                                        <div class="pull-right">
                                            {{--{{$applications->links()}}--}}

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection